export class Produto{
    id!: string;            // !deixa como opcional
    nome!: string;
    valor!: string;
    promocao!: boolean;
    valorPromo!: string;
    imagem!: string;
}